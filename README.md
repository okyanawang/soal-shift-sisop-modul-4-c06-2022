# soal-shift-sisop-modul-4-C06-2022
## Laporan Pengerjaan Modul 4 Praktikum Sistem Operasi    

### Nama Anggota Kelompok:  
1. Nadya Permata Sari
2. Aaliyah Farah Adibah
3. Okyan Awang Ramadhana

# Soal 1
```c
static struct fuse_operations xmp_oper = {
	.getattr = xmp_getattr,
	.readdir = xmp_readdir,
	.read = xmp_read,
	.mkdir = xmp_mkdir,
	.rmdir = xmp_rmdir,
	.rename = xmp_rename,
	.truncate = xmp_truncate,
	.write = xmp_write,
	.create = xmp_create,
	.utimens = xmp_utimens,
	.access = xmp_access,
	.open = xmp_open,
	.unlink = xmp_unlink,
	.readlink = xmp_readlink,
	.mknod = xmp_mknod,
	.symlink = xmp_symlink,
	.link = xmp_link,
	.chmod = xmp_chmod,
	.chown = xmp_chown,
	.statfs = xmp_statfs,
};  
```

struktur fuse yang kami gunakan adalah seperti diatas :

getattr = Untuk mengembalikan attribute dari suatu file  
readdir = Untuk melakukan pembacaan directory  
read = untuk melakukan pembacaan file  
mkdir = untuk pembuatan suatu directory baru  
rmdir = untuk menghapus directory  
rename = untuk menganti nama sebuah directory ataupun file  
truncate = perpanjang file yang diberikan sehingga ukurannya tepat beberapa byte  
write = penulisan ke suatu file atau directory  
open = membuka file  
statfs = mengembalikan statistic dari file yang digunakan  
unlink = menghapus simbolik link dan file yang diberikan  
readlink = Jika jalur adalah tautan simbolik, isi buf dengan targetnya, hingga ukuran tertentu.  
mknod = Buat file (perangkat) khusus, FIFO, atau soket.  
symlink = Buat tautan simbolik bernama "dari" yang, ketika dievaluasi, akan mengarah ke "ke".  
link = Buat hardlink antara "dari" dan "ke".  
chmod = memodifikasi izin  
chown = mengganti kepemilikan file

a. Jika sebuah direktori dibuat dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode.  

```c
static int xmp_mkdir(const char *path, mode_t mode){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) {dekripsiMenjadiAtbash(a);dekripsiMenjadiRot13(a);};
	
	char *b = strstr(path, ian);
	if (b != NULL){desripsiMenjadiVigenere(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	result = mkdir(filepath, mode);
	writeTheLog("MKDIR", filepath);

	if (result == -1) return -errno;
	return 0;
}
```
Ketika menggunakan mkdir, akan dibandingkan dengan strstr apakah direktory skrng terdapat variabel animeku(isinya Ani,eku). jika terdapat panggil fungsi dekripsiMenjadiAtbash(a) lalu masukkan ke log dengan memanggil fungsi writeTheLog("MKDIR", filepath)

b. Jika sebuah direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode.  
c. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.  
```c
static int xmp_rename(const char *from, const char *to){
	int result;
	char dariDir[1000], keDir[1000];
	
	char *a = strstr(to, anime);
	if (a != NULL) {dekripsiMenjadiAtbash(a);dekripsiMenjadiRot13(a);};
	
	char *b = strstr(from, ian);
	if (b != NULL){desripsiMenjadiVigenere(b);}
	
	char *c = strstr(to, ian);
	if (c != NULL){dekripsiMenjadiRot13(c);dekripsiMenjadiAtbash(c);}

	sprintf(dariDir, "%s%s", dirPath, from);
	sprintf(keDir, "%s%s", dirPath, to);

	result = rename(dariDir, keDir);
	if (result == -1) return -errno;

	writeTheLog2("RENAME", dariDir, keDir);
	
	if (c != NULL){lakukanEnkripsi(keDir);writeTheLog2("ENCRYPT2", from, to);}
	if (b != NULL && c == NULL){lakukanDekripsi(keDir);writeTheLog2("DECRYPT2", from, to);}
	if (strstr(to, namdo) != NULL){encryptBinary(keDir);writeTheLog2("ENCRYPT3", from, to);}
	if (strstr(from, namdo) != NULL && strstr(to, namdo) == NULL){decryptBinary(keDir);writeTheLog2("DECRYPT3", from, to);}

	return 0;
}
```
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) {dekripsiMenjadiAtbash(a);dekripsiMenjadiRot13(a);};
	
	char *b = strstr(path, ian);
	if (b != NULL){desripsiMenjadiVigenere(b);}

	if (strcmp(path, "/") == 0){path = dirPath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirPath, path);}

	if (x != 24) x++;
	else writeTheLog("READDIR", filepath);

	int result = 0;
	DIR *dp;
	struct dirent *de;

	(void)offset;
	(void)fi;

	dp = opendir(filepath);
	if (dp == NULL) return -errno;

	while ((de = readdir(dp)) != NULL){
		if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;

		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;

		if (a != NULL) enkripsiMenjadiAtbash(de->d_name);
		if (b != NULL){enkripsiMenjadiAtbash(de->d_name);enkripsiMenjadiRot13(de->d_name);}

		result = (filler(buf, de->d_name, &st, 0));
		if (result != 0) break;
	}

	closedir(dp);
	return 0;
}
```
Ketika dipanggil rename, dia akan membandingkan file apakah pada nama file terdapat "Animeku" jika terdapat lakukan deskripsi atbash dengan memanggil fungsi lakukanEnkripsi(keDir) lalu tulis ke dalam log RENAME dan dari posisi mana ke mana
jika direname kembali dengan menghilangkan Animeku maka dia akan kembali kefungsi semula dengan memanggil fungsi deskripsiAtbash


Berikut adalah penjelasan bagaimana fungsi enkripsi dan deskripsi bekerja

Pada bagian ini kami diminta untuk melakukan enkripsi maupun dekripsi terhadap nama file dan nama folder menggunakan Atbash cipher untuk Uppercase dan rot13 untuk lowercase. Untuk mendapatkan nama file dan nama folder yang diinginkan, kelompok kami melakukan looping untuk melakukan pengecekkan posisi awal berupa slash (/) dan posisi akhir berupa titik (.). Disini kami menerapkan 3 fungsi untuk mendapatkan index awal berupa slash (/) dan berupa titik (.) enkripsi dan dekripsi:
```c
int split_ext_id(char *path){
	int ada = 0;
	for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.'){
			if(ada == 1) return i;
			else ada = 1;
		}
	}
	return strlen(path);
}
```
Fungsi diatas dipanggil untuk mengembalikan index file extension pada file yang sudah dipisahkan. Pada program tersebut akan dilakukan perulangan sepanjang length dari path yang dipassing. setelah ketemu "." maka akan direturn titiknya berada pada posisi berapa
```c
int ext_id(char *path){
	for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.') return i;
	}
	return strlen(path);
}
```
fungsi diatas dipanggil untuk Mengembalikan index file extension berada pada posisi keberapa
```c
int slash_id(char *path, int mentok){
	for(int i=0; i<strlen(path); i++){
		if (path[i] == '/') return i + 1;
	}
	return mentok;
}
```
fungsi diatas dipanggil untuk Mengembalikan index slash berada pada posisi ke berapa. Inti dari program ini adalah melakukan perulangan pada path yang diterima. jika slash terdeteksi akan dikembalikan posisi slash+1

untuk melakukan enkripsi dan deskripsi seperti yang sudah dijelaskan diatas, saya menggunakan kode berikut ini :
```c
void encryptAtbash(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	printf("encrypt path Atbash: %s\n", path);
	
	int endid = split_ext_id(path);
	if(endid == strlen(path)) endid = ext_id(path);
	int startid = slash_id(path, 0);
	
	for (int i = startid; i < endid; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char tmp = path[i];
			if(isupper(path[i])) tmp -= 'A';
			else tmp -= 'a';
			tmp = 25 - tmp; //Atbash cipher
			if(isupper(path[i])) tmp += 'A';
			else tmp += 'a';
			path[i] = tmp;
		}
	}
}

void decryptAtbash(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	printf("decrypt path Atbash: %s\n", path);
	
	int endid = split_ext_id(path);
	if(endid == strlen(path)) endid = ext_id(path);
	int startid = slash_id(path, endid);
	
	for (int i = startid; i < endid; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char tmp = path[i];
			if(isupper(path[i])) tmp -= 'A';
			else tmp -= 'a';
			tmp = 25 - tmp; //Atbash cipher
			if(isupper(path[i])) tmp += 'A';
			else tmp += 'a';
			path[i] = tmp;
		}
	}
}
```

<br>
<strong>Dokumentasi : </strong>
<img src="asset/4-1a.png" alt="1.1">
<img src="asset/4-1b.png" alt="1.2">

# Soal 2
- Menggunakan fungsi strstr(), akan dilakukan pengecekkan apakah suatu direktori dilakukan rename dengan menambahkan IAN_ ataupun penghapusan IAN_. Setelah kelompok kami lakukan pengecekkan apakah di dalam path tersebut ada string IAN_. Jika ditemukan adanya string tersebut, maka akan dilakukan enkripsi filenya menggunakan vignere cipher sesuai dengan perintah soal. Hal yang sama akan diterapkan pada fungsi dekripsinya yaitu jika pada awalnya ada IAN_ kemudian dilakukan penghapusan string IAN_, maka akan dilakukan dekripsi pada filenya menggunakan vignere cipher sesuai dengan perintah soal.  

```c
static int xmp_rename(const char *from, const char *to){
	int result;
	char dariDir[1000], keDir[1000];
	
	char *a = strstr(to, anime);
	if (a != NULL) {dekripsiMenjadiAtbash(a);dekripsiMenjadiRot13(a);};
	
	char *b = strstr(from, ian);
	if (b != NULL){desripsiMenjadiVigenere(b);}
	
	char *c = strstr(to, ian);
	if (c != NULL){dekripsiMenjadiRot13(c);dekripsiMenjadiAtbash(c);}

	sprintf(dariDir, "%s%s", dirPath, from);
	sprintf(keDir, "%s%s", dirPath, to);

	result = rename(dariDir, keDir);
	if (result == -1) return -errno;

	writeTheLog2("RENAME", dariDir, keDir);
	
	if (c != NULL){lakukanEnkripsi(keDir);writeTheLog2("ENCRYPT2", from, to);}
	if (b != NULL && c == NULL){lakukanDekripsi(keDir);writeTheLog2("DECRYPT2", from, to);}
	if (strstr(to, namdo) != NULL){encryptBinary(keDir);writeTheLog2("ENCRYPT3", from, to);}
	if (strstr(from, namdo) != NULL && strstr(to, namdo) == NULL){decryptBinary(keDir);writeTheLog2("DECRYPT3", from, to);}

	return 0;
}
```
- Jika terdeteksi adanya string IAN_ pada path tujuan berarti telah dilakukan rename dengan menambahkan IAN_ sehingga akan dilakukan dekripsi pada file menggunakan atbash dan rot13 cipher yang kemudian akan dilanjutkan dengan melakukan pemecahan file menjadi berukuran 1024 byte sesuai dengan perintah pada soal.
```c
void lakukanEnkripsi(char *filepath){
	chdir(filepath);
	DIR *dp;
	struct dirent *dir;
	
	dp = opendir(".");
	if(dp == NULL) return;
	struct stat lol;
	char dirPath[1000];
	char filePath[1000];
	
    while ((dir = readdir(dp)) != NULL){
		if (stat(dir->d_name, &lol) < 0);
		else if (S_ISDIR(lol.st_mode)){
			if (strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..") == 0) continue;sprintf(dirPath,"%s/%s",filepath, dir->d_name);lakukanEnkripsi(dirPath);
		}
		else{
			sprintf(filePath,"%s/%s",filepath,dir->d_name);
			FILE *input = fopen(filePath, "r");
			if (input == NULL) return;
			int baca_byte, hitung = 0;
			void *buffer = malloc(1024);
			while((baca_byte = fread(buffer, 1, 1024, input)) > 0){
				char newFilePath[1000];
				sprintf(newFilePath, "%s.%04d", filePath, hitung);
				hitung++;
				FILE *keluaranEnc = fopen(newFilePath, "w+");
				if(keluaranEnc == NULL) return;
				fwrite(buffer, 1, baca_byte, keluaranEnc);
				fclose(keluaranEnc);
				memset(buffer, 0, 1024);
			}
			fclose(input);
			remove(filePath);
		}
	}
    closedir(dp);
}
```

- Jika terdeteksi string IAN_ pada path asal dan tidak terdeteksi string IAN_ pada path tujuan berarti telah dilakukan rename dengan menghilangkan IAN_ sehingga akan dilakukan penggabungan file menjadi satu kesatuan dan kemudian dilakukan dekripsi menggunakan vignere cipher pada file tersebut.    
```c
void lakukanDekripsi(char *dir){
	chdir(dir);
	DIR *dp;
	struct dirent *de;
	struct stat lol;
	dp = opendir(".");
	if (dp == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	
	while ((de = readdir(dp)) != NULL){
		if (stat(de->d_name, &lol) < 0);
		else if (S_ISDIR(lol.st_mode)){
			if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;sprintf(dirPath, "%s/%s", dir, de->d_name);lakukanDekripsi(dirPath);
		}
		else{
			sprintf(filePath, "%s/%s", dir, de->d_name);
			filePath[strlen(filePath) - 5] = '\0';
			FILE *check = fopen(filePath, "r");
			if (check != NULL) return;
			FILE *file = fopen(filePath, "w");
			int hitung = 0;
			char keDir[1000];
			sprintf(keDir, "%s.%04d", filePath, hitung);
			void *buffer = malloc(1024);
			while (1){
				FILE *op = fopen(keDir, "rb");
				if (op == NULL) break;
				size_t readSize = fread(buffer, 1, 1024, op);
				fwrite(buffer, 1, readSize, file);
				fclose(op);
				remove(keDir);
				hitung++;
				sprintf(keDir, "%s.%04d", filePath, hitung);
			}
			free(buffer);
			fclose(file);
		}
	}
	closedir(dp);
}
```

- Untuk melakukan enkripsi dan dekripsi menggunakan vignere cipher akan dilakukan oleh fungsi berikut:
```c
void enkripsiMenjadiRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	
	int idAkhir = pisahkanExt(path);
	int idAwal = pemisahId(path, 0);
	
	for (int i = idAwal; i < idAkhir; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char sementara = path[i];
			if(isupper(path[i])) sementara -= 'A'; else sementara -= 'a'; sementara = (sementara + 13) % 26;
			if(isupper(path[i])) sementara += 'A'; else sementara += 'a';
			path[i] = sementara;
		}
	}
}

void dekripsiMenjadiRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	
	int idAkhir = pisahkanExt(path);
	int idAwal = pemisahId(path, idAkhir);
	
	for (int i = idAwal; i < idAkhir; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char sementara = path[i];
			if(isupper(path[i])) sementara -= 'A'; else sementara -= 'a'; sementara = (sementara + 13) % 26;
			if(isupper(path[i])) sementara += 'A'; else sementara += 'a';
			path[i] = sementara;
		}
	}
}
```
- Pemanggilan fungsi dekripsi dilakukan pada tiap utility functions seperti getattr, mkdir, rename, rmdir, create, dan fungsi-fungsi lain yang menurut kelompok kami sering digunakan dalam proses sinkronisasi FUSE dan mount folder. Fungsi dekripsi dan enkripsi dilakukan di utility function readdir karena FUSE akan melakukan dekripsi di mount folder lalu enkripsi di FUSE saat readdir. Pemanggilannya dilakukan dengan melakukan pengecekan apakah string IAN_ terdapat di string path di masing-masing utility function dengan menggunakan fungsi strstr(). Jika ditemukan adanya string IAN_, maka fungsi enkripsi dan dekripsi akan dipanggil untuk string tersebut dengan IAN_ sebagai starting point string yang diteruskan. Untuk pencatatan running log akan dijelaskan pada bagian nomor 4.

<br>
<strong>Dokumentasi : </strong>
<img src="asset/4-2a.png" alt="2.1">
<img src="asset/4-2b.png" alt="2.2">

# Soal 3

- Untuk soal ini kelompok kami membuat utility functions RENAME direktori yang akan dilakukan pengecekan apakah direktori direname dengan menambahkan nam_do-saq_ dan menjadi direktori spesial atau menghilangkan nam_do-saq_ dan menjadi direktori biasa dengan fungsi strstr() yang dipanggil.
```c
static int xmp_rename(const char *from, const char *to){
	int result;
	char dariDir[1000], keDir[1000];
	
	char *a = strstr(to, anime);
	if (a != NULL) {dekripsiMenjadiAtbash(a);dekripsiMenjadiRot13(a);};
	
	char *b = strstr(from, ian);
	if (b != NULL){desripsiMenjadiVigenere(b);}
	
	char *c = strstr(to, ian);
	if (c != NULL){dekripsiMenjadiRot13(c);dekripsiMenjadiAtbash(c);}

	sprintf(dariDir, "%s%s", dirPath, from);
	sprintf(keDir, "%s%s", dirPath, to);

	result = rename(dariDir, keDir);
	if (result == -1) return -errno;

	writeTheLog2("RENAME", dariDir, keDir);
	
	if (c != NULL){lakukanEnkripsi(keDir);writeTheLog2("ENCRYPT2", from, to);}
	if (b != NULL && c == NULL){lakukanDekripsi(keDir);writeTheLog2("DECRYPT2", from, to);}
	if (strstr(to, namdo) != NULL){encryptBinary(keDir);writeTheLog2("ENCRYPT3", from, to);}
	if (strstr(from, namdo) != NULL && strstr(to, namdo) == NULL){decryptBinary(keDir);writeTheLog2("DECRYPT3", from, to);}

	return 0;
}
```
- Selanjutnya terdeteksi direktori bernama nam_do-saq_ pada path tujuan berarti direktori direname dengan menambahkan nam_do-saq_ pada nama direktori . Maka akan dilanjutkan dengan mengubah nama file menjadi lowercase dan menambahkan nilai desimalnya sebagai ekstensi yang baru pada fungsi encryptBinary pada bagian file.
```c
void ambilBiner(char *fname, char *bin, char *lowercase){
	int idAkhir = extensionId(fname);
	int idAwal = pemisahId(fname, 0);
	int i;
	
	for(i=idAwal; i<idAkhir; i++){
		if(isupper(fname[i])){
			bin[i] = '1';lowercase[i] = fname[i] + 32;
		}
		else{
			bin[i] = '0';lowercase[i] = fname[i];
		}
	}
	bin[idAkhir] = '\0';
	
	for(; i<strlen(fname); i++){
		lowercase[i] = fname[i];
	}
	lowercase[i] = '\0';
}

int bin2dec(char *bin){
	int sementara = 1, result = 0;
	for(int i=strlen(bin)-1; i>=0; i--){if(bin[i] == '1') result += sementara; sementara *= 2;}
	return result;
}

void encryptBinary(char *filepath){
	chdir(filepath);
	DIR *dp;
	struct dirent *dir;
	struct stat lol;
	dp = opendir(".");
	if(dp == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char filePathBinary[1000];
	
    while ((dir = readdir(dp)) != NULL){
		if (stat(dir->d_name, &lol) < 0);
		else if (S_ISDIR(lol.st_mode)){
			if (strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..") == 0) continue;sprintf(dirPath,"%s/%s",filepath, dir->d_name);encryptBinary(dirPath);
		}else{
			sprintf(filePath,"%s/%s",filepath, dir->d_name);
			char bin[1000], lowercase[1000]; ambilBiner(dir->d_name, bin, lowercase);
			int dec = bin2dec(bin);
			sprintf(filePathBinary,"%s/%s.%d",filepath,lowercase,dec); rename(filePath, filePathBinary);
		}
	}
    closedir(dp);
}
```
- Selanjutnya apabila terdeteksi direktori nam_do-saq_ pada path asal dan tidak terdeteksi adanya nam_do-saq_ pada path tujuan berarti direktori direname dengan menghilangkan nam_do-saq_ maka direktori tersebut akan kembali menjadi direktori awal. Maka akan dilanjutkan dengan mengubah nama file menjadi semula dengan bantuan nilai desimalnya pada fungsi decryptBinary.
```c
int convertDec(char *ext){
	int dec = 0, pengali = 1;
	for(int i=strlen(ext)-1; i>=0; i--){dec += (ext[i]-'0')*pengali;pengali *= 10;}
	return dec;
}

void dec2bin(int dec, char *bin, int len){
	int idx = 0;
	while(dec){
		if(dec & 1) bin[idx] = '1';
		else bin[idx] = '0';idx++;
		dec /= 2;
	}
	while(idx < len){
		bin[idx] = '0'; idx++;
	}
	bin[idx] = '\0';
	
	for(int i=0; i<idx/2; i++){
		char sementara = bin[i];bin[i] = bin[idx-1-i];bin[idx-1-i] = sementara;
	}
}

void getDecimal(char *fname, char *bin, char *normalcase){
	int idAkhir = extensionId(fname);
	int idAwal = pemisahId(fname, 0);
	int i;
	
	for(i=idAwal; i<idAkhir; i++){
		if(bin[i-idAwal] == '1') normalcase[i-idAwal] = fname[i] - 32;
		else normalcase[i-idAwal] = fname[i];
	}
	
	for(; i<strlen(fname); i++){
		normalcase[i-idAwal] = fname[i];
	}
	normalcase[i-idAwal] = '\0';
}

void decryptBinary(char *filepath){
	chdir(filepath);
	DIR *dp;
	struct dirent *dir;
	struct stat lol;
	dp = opendir(".");
	if(dp == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char filePathDecimal[1000];
	
    while ((dir = readdir(dp)) != NULL){
		if (stat(dir->d_name, &lol) < 0);
		else if (S_ISDIR(lol.st_mode)){
			if (strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..") == 0) continue;sprintf(dirPath,"%s/%s",filepath, dir->d_name);decryptBinary(dirPath);
		}
		else{
			sprintf(filePath,"%s/%s",filepath, dir->d_name);
			char fname[1000], bin[1000], normalcase[1000], clearPath[1000];
			
			strcpy(fname, dir->d_name);
			char *ext = strrchr(fname, '.');
			int dec = convertDec(ext+1);
			for(int i=0; i<strlen(fname)-strlen(ext); i++) clearPath[i] = fname[i];
			
			char *ext2 = strrchr(clearPath, '.');
			dec2bin(dec, bin, strlen(clearPath)-strlen(ext2));getDecimal(clearPath, bin, normalcase);sprintf(filePathDecimal,"%s/%s",filepath,normalcase);rename(filePath, filePathDecimal);
		}
	}
    closedir(dp);
}
```

<br>
<strong>Dokumentasi : </strong>
<img src="asset/4-3a.png" alt="3.1">
<img src="asset/4-3b.png" alt="3.2">
<img src="asset/4-3c.png" alt="3.3">

## Kendala yang Dihadapi
-  kesulitan dalam mengambil waktu sekarang (real time) dan pada pencatatannya.
-  bingung untuk mencatat System Call karena ada yang punya 1 atau 2 argumen. Akhirnya kami pisahkan menjadi 2 fungsi untut mencatat log.
-  Terkadang file file yang ada di Downloads tidak muncul semuanya ketika sudah dimount
-  Contoh kode dari asisten di github, sangat sederhana sehingga kami tidak punya acuan bagaimana cara mengoding dengan baik dan benar. Di internet juga referensinya sangat sedikit
-  Kebingungan memaknai folder special / directory special itu seperti apa